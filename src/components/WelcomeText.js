import React, { useEffect, useState } from 'react';
import UserLocation from './Location/UserLocation';

const WelcomeText = () => {
    const [userLocation, setLocation] = useState('');
   
    const getLocation = async (userLocation = null) => {
        const endpoint = 'location';
        const response = await UserLocation.get(`/${endpoint}`);
        setLocation(response.data);
    };

    useEffect(() => {
        getLocation();
      }, []);

    if(userLocation === 'MX' || userLocation === 'US'){
        return(
            <div>
                <h1>
                    { userLocation === 'MX' ? '¿Cuántos tacos quieres?' : 'How many tacos do you want?'}
                </h1>
           </div>
        )
    } else {
        return(
            <div>
                <h1>Hi, I only works on MX and US :p</h1>
           </div>
        )
    }   
}

export default WelcomeText;
